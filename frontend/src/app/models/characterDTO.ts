import { character } from './character';

export class characterDTO {
  total!: number;
  page!: number;
  lastPage!: number;
  data!: character[];
}
