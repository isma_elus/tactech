import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CharactersService } from 'src/app/services/characters.service';

@Component({
  selector: 'app-character-details',
  templateUrl: './character-details.component.html',
  styleUrls: ['./character-details.component.css'],
})
export class CharacterDetailsComponent implements OnInit {
  currentCharacter: any;

  @Input() public idModal: string = '';

  constructor(
    private characterService: CharactersService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  // ngOnInit(): void {}

  ngOnInit(): void {
    // this.getCharacter(this.route.snapshot.paramMap.get('id'));
    this.getCharacter(this.idModal);
  }

  getCharacter(id: any): void {
    this.currentCharacter = this.characterService.getCharacter(id).subscribe(
      (response) => {
        const { data } = response;
        this.currentCharacter = data[0];
      },
      (error) => console.log(error)
    );
  }
}
