import { characterDTO } from './../models/characterDTO';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { character } from '../models/character';

@Injectable({
  providedIn: 'root',
})
export class CharactersService {
  constructor(private _httpClient: HttpClient) {}

  getCharacters(params: HttpParams) {
    return this._httpClient.get<characterDTO>(
      `${environment.URL_API}/characters`,
      { params: params }
    );
  }

  getCharacter(id: string) {
    return this._httpClient.get<characterDTO>(
      `${environment.URL_API}/characters/${id}`
    );
  }
}
