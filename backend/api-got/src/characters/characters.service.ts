import { Character, CharacterDocument } from './character.entity';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { HttpService } from '@nestjs/axios';
import { map } from 'rxjs/operators';

@Injectable()
export class CharactersService {
  constructor(
    @InjectModel(Character.name)
    private readonly characterModel: Model<CharacterDocument>,
    private httpService: HttpService,
  ) {}

  find(options) {
    return this.characterModel.find(options);
  }

  findId(id: string) {
    return this.characterModel.findById(id).exec();
  }

  count(options) {
    return this.characterModel.countDocuments(options).exec();
  }

  async getData() {
    const datosJson = this.httpService
      .get('https://api.got.show/api/book/characters', {
        headers: { Accept: 'application/json' },
      })
      .pipe(
        map((response) =>
          response.data.map((character) => {
            return {
              name: character.name,
              titles: character.titles,
              books: character.books,
              gender: character.gender,
              house: character.house,
              slug: character.slug,
              image: character.image,
            };
          }),
        ),
      );

    return datosJson.forEach((value) => {
      value.forEach((character) => {
        const newCharacter = new this.characterModel(character);
        newCharacter.save();
      });
    });
  }
}
