import { Controller, Get, Param, Req } from '@nestjs/common';
import { CharactersService } from './characters.service';
import { Request } from 'express';

@Controller('characters')
export class CharactersController {
  constructor(private readonly characterService: CharactersService) {}

  @Get('/GetData')
  async getData() {
    return this.characterService.getData();
  }

  @Get(':id')
  async getCharacterByid(@Param('id') id: string) {
    const options = { _id: id };
    const query = this.characterService.find(options);
    const data = await query.exec();
    return { data };
  }

  @Get()
  async getCharacter(@Req() req: Request) {
    let options = {};

    if (req.query.name) {
      options = {
        $or: [{ name: new RegExp(req.query.name.toString(), 'i') }],
      };
    }

    if (req.query.house) {
      options = {
        $or: [{ house: new RegExp(req.query.house.toString(), 'i') }],
      };
    }

    const query = this.characterService.find(options);

    const page: number = parseInt(req.query.page as any) || 1;
    const limit = 10;
    const total = await this.characterService.count(options);

    const data = await query
      .skip((page - 1) * limit)
      .limit(limit)
      .exec();

    return { data, total, page, lastPage: Math.ceil(total / limit) };
  }
}
