import { Component, OnInit } from '@angular/core';
import { CharactersService } from 'src/app/services/characters.service';
import { CharacterDetailsComponent } from '../character-details/character-details.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-characters-list',
  templateUrl: './characters-list.component.html',
  styleUrls: ['./characters-list.component.css'],
})
export class CharactersListComponent implements OnInit {
  characters: any;
  currentCharacter = null;
  currentIndex = 1;
  search = '';

  currentPage = 1;
  count = 0;
  pageSize = 10;
  lastPage = 1;

  constructor(
    private characterService: CharactersService,
    private modalService: NgbModal
  ) {}

  openModal(idModal: string): void {
    const modalRef = this.modalService.open(CharacterDetailsComponent);
    modalRef.componentInstance.idModal = idModal;
  }

  ngOnInit(): void {
    this.getCharacter();
  }

  getParams(search: any, page: any): any {
    let params = { page: page };
    if (search) {
      Object.assign(params, { home: search });
      Object.assign(params, { name: search });
    }

    return params;
  }

  getCharacter(): void {
    const params = this.getParams(this.search, this.currentPage);

    this.characters = this.characterService.getCharacters(params).subscribe(
      (response) => {
        const { data, total, page, lastPage } = response;
        this.characters = data;
        this.count = total;
        this.currentPage = page;
        this.lastPage = lastPage;
      },
      (error) => console.log(error)
    );
  }

  handlePageChange(event: any): void {
    this.currentPage = event;
    this.getCharacter();
  }

  getCharacterSearch() {
    this.currentPage = 1;
    this.getCharacter();
  }
}
