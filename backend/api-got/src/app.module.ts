import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CharactersModule } from './characters/characters.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/apigot', { autoCreate: true }),
    CharactersModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
