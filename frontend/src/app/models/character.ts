export class character {
  _id!: string;
  name!: string;
  slug!: string;
  gender!: string;
  house!: string;
  books!: string[];
  titles!: string[];
  image!: string;
}
