import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type CharacterDocument = Character & Document;

@Schema()
export class Character {
  @Prop()
  name: string;
  @Prop()
  slug: string;
  @Prop()
  gender: string;
  @Prop()
  house: string;
  @Prop()
  books: string[];
  @Prop()
  titles: string[];
  @Prop()
  image: string;
}

export const CharacterSchema = SchemaFactory.createForClass(Character);
